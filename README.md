# Nested Boxes

Given a set of boxes, checks which ones can contain which ones,
build a digraph, topologically sort it and display the result,
making it easy to find the longest chain of boxes.

## Example

```shell
cat <<EOF | python3 -m boxes
37, 40, 48
33, 45, 51
26, 57, 45
47, 38, 24
40, 25, 18
38, 19, 14
20, 14, 14
EOF
```

![example.png](example.png)
