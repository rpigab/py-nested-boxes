import fileinput
import re

PATTERN = re.compile(r'(\d+)\D+(\d+)\D+(\d+)')


def extract_tuples(input_string: str) -> (int, int, int):
    match = PATTERN.findall(input_string)[0]
    return int(match[0]), int(match[1]), int(match[2])


def get_tuples_from_input(input_file: str) -> [(int, int, int)]:
    return [extract_tuples(line) for line in fileinput.input(input_file)]
