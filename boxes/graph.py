import networkx as nx
from matplotlib import pyplot

from boxes.box import Box
import itertools


class Graph:

    def __init__(self, boxes: [Box]):
        # Create a directed acyclic graph where edges represent containment relationships
        # Use the canContain method to determine relationships
        self.g = nx.DiGraph()

        for b1, b2 in itertools.combinations(boxes, 2):
            if b1.can_contain(b2):
                self.g.add_edge(b1, b2)

    def draw_graph(self):
        pos = nx.circular_layout(self.g)
        nx.draw(self.g, pos, node_size=4000, node_color="cyan",
                with_labels=True, arrowsize=30)
        pyplot.show()

    def topological_sort_and_display(self, output_img_filename: str):
        for layer, nodes in enumerate(nx.topological_generations(self.g)):
            # `multipartite_layout` expects the layer as a node attribute, so add the
            # numeric layer value as a node attribute
            for node in nodes:
                self.g.nodes[node]["layer"] = layer

        # remove edges that are not direct neighbors after topological sorting
        nodes_and_layers = nx.get_node_attributes(self.g, "layer").items()
        edges_to_remove = []
        for node, node_layer in nodes_and_layers:
            for next_node in self.g.successors(node):
                res = [[next_node_distant, next_node_layer] for next_node_distant, next_node_layer in nodes_and_layers
                       if next_node_distant == next_node and next_node_layer >= node_layer + 2]
                if res:
                    next_node_to_remove_edge_from = res[0][0]
                    edges_to_remove.append([node, next_node_to_remove_edge_from])
        self.g.remove_edges_from(edges_to_remove)

        # Compute the multipartite_layout using the "layer" node attribute
        pos = nx.multipartite_layout(self.g, subset_key="layer")
        _, ax = pyplot.subplots(layout='constrained')
        ax.axis('off')
        nx.draw_networkx(self.g,
                         pos=pos, ax=ax,
                         # Font
                         font_size=12,
                         font_color='000',
                         # Nodes
                         node_size=4000,
                         node_color='cyan',
                         # Arrows
                         arrowstyle="->",
                         arrowsize=25,
                         width=3)

        # pyplot.show()
        pyplot.savefig(output_img_filename, bbox_inches='tight')
