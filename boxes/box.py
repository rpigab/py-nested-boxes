import logging
from dataclasses import dataclass

MIN_OFFSET = 0.5


@dataclass(frozen=True)
class Box:
    x: int
    y: int
    z: int

    @staticmethod
    def from_tuple(tup: [int, int, int]):
        [x, y, z] = tup
        box = Box(x, y, z)
        logging.info(f'creating new box: {box}')
        return box

    def __str__(self):
        return f'{self.x}*{self.y}*{self.z}'

    def can_contain(self, other: 'Box') -> bool:
        # Check if self box can contain the other box, considering rotations

        # Check the original orientation
        if self.x > other.x + MIN_OFFSET and self.y > other.y + MIN_OFFSET and self.z > other.z + MIN_OFFSET:
            return True

        # Check rotated orientations (all possible permutations)
        rotations = [
            [self.x, self.y, self.z],
            [self.x, self.z, self.y],
            [self.y, self.x, self.z],
            [self.y, self.z, self.x],
            [self.z, self.x, self.y],
            [self.z, self.y, self.x],
        ]

        for rotation in rotations:
            [rx, ry, rz] = rotation

            if rx > other.x + MIN_OFFSET and ry > other.y + MIN_OFFSET and rz > other.z + MIN_OFFSET:
                return True

        return False
