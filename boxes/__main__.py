import argparse

from boxes.box import Box
from boxes.graph import Graph
from boxes.input import get_tuples_from_input
from boxes.setup_logging import setup_logging


class Args:
    input_filename: str
    output_img_filename: str


if __name__ == '__main__':
    # Logging
    setup_logging()

    # Arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('input_filename')
    parser.add_argument('output_img_filename', nargs='?', default='data/plot.png')
    args = Args()
    parser.parse_args(namespace=args)

    # Create boxes, graph and output result
    boxes = [Box.from_tuple(tup) for tup in get_tuples_from_input(args.input_filename)]
    graph = Graph(boxes)
    graph.topological_sort_and_display(args.output_img_filename)
