import logging


def setup_logging():
    # Configure the logging format
    log_format = '%(asctime)s [%(levelname)s] %(message)s'
    log_date_format = '%Y-%m-%d %H:%M:%S'

    # Create and configure a logger
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # Create a formatter and set the date format
    formatter = logging.Formatter(log_format, datefmt=log_date_format)

    # Create a console handler and add the formatter to it
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)

    # Add the console handler to the logger
    logger.addHandler(console_handler)
    return logger
