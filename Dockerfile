FROM python:3-slim-buster

WORKDIR /app

ARG APP_USER=app
ARG UID=1000

RUN useradd -m -u "${UID}" -r "$APP_USER" && \
    chown "$APP_USER" /app

# Dependencies
COPY requirements.txt .
RUN pip install -r requirements.txt

# Source code
COPY boxes boxes

USER "$APP_USER"

# Entrypoint
ENTRYPOINT [ "python", "-m", "boxes" ]
CMD [ "data/input.txt" ]
